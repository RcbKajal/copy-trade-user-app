/* eslint-disable @typescript-eslint/naming-convention */
export const environment = {
  production: true,
  API_URL: 'https://copy-trade-backend.discoverypos.com',
  ASSETS_URL: 'https://copy-trade-backend.discoverypos.com',
  WS_URL: 'wss://copy-trade-backend.discoverypos.com',
  language: 'en',
};
