import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BgLoadDirective } from './bg-load.directive';
import { DivDirective } from './div.directive';


@NgModule({
  declarations: [BgLoadDirective, DivDirective],
  imports: [
    CommonModule
  ],
  exports: [BgLoadDirective, DivDirective]
})
export class DirectiveModule { }
