import { Directive, ElementRef, Input, OnInit, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[appBgLoad]'
})
export class BgLoadDirective implements OnInit {

  @Input() url: any;

  constructor(private ele: ElementRef) {
    this.ele.nativeElement.style.backgroundImage = 'url(\'assets/images/user/dummy-user.png\')';
    this.ele.nativeElement.style.backgroundPosition = 'center';
    this.ele.nativeElement.style.backgroundRepeat = 'no-repeat';
    this.ele.nativeElement.style.backgroundSize = 'cover';
  }

  ngOnInit() {
    this.onLoadData();
  }

  onLoadData() {
    if (this.url) {
      fetch(this.url).then((res) => {
        if(res.ok){
          this.ele.nativeElement.style.backgroundImage = 'url(' + this.url + ')';
        }
      }).catch((e) => {
        console.log(e);
      });
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.onLoadData();
  }
}
