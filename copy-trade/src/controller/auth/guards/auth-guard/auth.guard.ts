import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';

import { AuthenticationService } from '../../authentication.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StoreService } from 'src/controller/database/store.service';
import { User } from 'src/controller/app/models/User.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  constructor(private authService: AuthenticationService, private router: Router) {}
  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const loggedInCall = this.authService.isLoggedIn();
    return Promise.all([loggedInCall]).then((values) => {

      const loggedIn = values[0];
      if (loggedIn) {

        const currentUser = StoreService.get('User');
        if (currentUser === null) {
          this.authService.logout();
          return true;
        }

        if(currentUser) {
          this.authService.user = new User();
          this.authService.user.set(currentUser, true);
          if(this.authService.user?.isCopier) {
            this.router.navigateByUrl('/copier');
          }
          if(this.authService.user?.isSignalProvider) {
            this.router.navigateByUrl('/signal-provider');
          }
        }
        return false;
      }
      return true;
    });
  }
}
