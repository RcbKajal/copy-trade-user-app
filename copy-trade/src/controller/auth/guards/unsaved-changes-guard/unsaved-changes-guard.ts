import { ActivatedRouteSnapshot, CanActivate, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';

import { AlertController } from '@ionic/angular';
import { Injectable } from '@angular/core';

type CanDeactivateType = Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree;

export interface CanComponentDeactivate {
  canDeactivate: () => CanDeactivateType;
}

@Injectable({
  providedIn: 'root'
})
export class unSaveChangeGuard implements CanDeactivate<CanComponentDeactivate> {
  constructor(public alertCrtl: AlertController) { }
  canDeactivate(
    component: CanComponentDeactivate,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): CanDeactivateType {

      if(!component.canDeactivate){
        return true
      }


    if (component.canDeactivate()) {

      return new Promise((resolve, reject) => {

        this.alertCrtl.create({
          cssClass: 'confirm_alert',
          header: 'Confirm Navigation',
          subHeader: '',
          message: "Your changes have not been saved. Are you sure you want to leave this page?",

          buttons: [
            {
              text: 'No',
              role: 'cancel',
              cssClass: 'secondary',
              handler: () => {
                resolve(false)
              }
            }, {
              text: 'Yes',
              handler: () => {
                resolve(true)
              }
            }
          ]
        }).then(simpl => {
          simpl.present()
        });
      })
    } else {
      return true
    }


  }

}
