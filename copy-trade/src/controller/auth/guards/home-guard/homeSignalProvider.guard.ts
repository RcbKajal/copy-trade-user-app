/* eslint-disable max-len */

import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';

import { AuthenticationService } from '../../authentication.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StoreService } from 'src/controller/database/store.service';
import { User } from 'src/controller/app/models/User.model';

@Injectable({
  providedIn: 'root'
})
export class HomeSignalProviderGuard implements CanActivate {
  constructor(private authService: AuthenticationService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const loggedInCall = this.authService.isLoggedIn();
    return Promise.all([loggedInCall]).then((values) => {
      const loggedIn = values[0];
      if (!loggedIn) {
        this.router.navigateByUrl('/');
        return false;
      }

      const currentUser = StoreService.get('User');
      if (!currentUser) {
        this.authService.logout();
        return false;
      }

      this.authService.user = new User();
      this.authService.user.set(currentUser, true);
      if(this.authService.user?.isCopier) {
        this.router.navigateByUrl('/copier');
        return false;
      }

      if(!this.authService.user?.isSignalProvider) {
        this.authService.logout();
        return false;
      }

      return true;
    });
  }
}
