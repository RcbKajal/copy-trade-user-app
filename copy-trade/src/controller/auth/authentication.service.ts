import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { NavigationRouteService } from '../utility/handler/navigation-route.service';
import { Platform } from '@ionic/angular';
import { RequestService } from '../utility/handler/request.service';
import { StateService } from '../app/data/state.service';
import { StoreService } from './../database/store.service';

import { ToastrService } from '../utility/ui/toastr.service';
import { UserModel } from '../app/models/User.model';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  authState = new BehaviorSubject(false);
  user: UserModel | null = null;

  constructor(
    private platform: Platform,
    private toastr: ToastrService,
    public store: StoreService,
    private requestService: RequestService,
    public navCtrl: NavigationRouteService,
    public request: RequestService,
    private stateService: StateService
  ) {
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
  }

  ifLoggedIn() {
    const response = this.stateService.hasLoggedIn;
    if (response === 'true') {
      this.authState.next(true);
    }
    return response;
  }

  getToken(): Promise<any> {
    return Promise.resolve(this.stateService.token);
  }

  isLoggedIn(): Promise<boolean> {
    return Promise.resolve(this.stateService.hasLoggedIn);
  }

  setLoggedIn(value: any) {
    this.user = new UserModel();
    this.user.set(value, true);
    this.stateService.token = value.token;
    this.stateService.hasLoggedIn = true;
    this.stateService.userID = this.user.id;
    this.stateService.user = this.user;
  }

  logout() {
    document.cookie.split(';').forEach(function (c) { document.cookie = c.replace(/^ +/, '').replace(/=.*/, '=;expires=' + new Date().toUTCString() + ';path=/'); });
    localStorage.clear();
    this.authState.next(false);
    this.navCtrl.goTo('/', {}, 'root');
  }

  loadCurrentUser() {
    const currentUser = StoreService.get('User');
    if (currentUser === null) {
      this.logout();
    }

    this.user = new UserModel();
    this.user.set(currentUser, true);
    this.getUserProfile();
  }

  getUserProfile(backgroundMode = false) {
    const data = {
      id: this.stateService.userID,
    };
    const success = (value: any) => {
      this.user?.set(value.result);
    };

    this.request.send('myProfileGet', data, success, null, backgroundMode);
  }

  updatePassword(data: any) {
    const success = (value: any) => {
      if (value.status !== 'OK') {
        this.toastr.error(value.message);
        return;
      }

      this.toastr.success(value.message);
    };

    this.request.send('updatePassword', data, success);
  }
  // copier
  async copierLogin(user: any) {

    const success = (value: any) => {
      this.setLoggedIn(value.data.user);
      this.toastr.success(value.message);
      this.navCtrl.goTo('/home', {}, 'root');
    };
    this.requestService.send('copierLogin', user, success);
  }

  async copierSignupSendOtp(user: any, response: any = null) {
    this.requestService.send('copierSignupSendOtp', user, response);
  }

  async copierSignup(user: any) {
    const success = (value: any) => {
      if (value.status === 'FAILED') {
        this.toastr.error(value.message);
        return;
      }
      if (value.status == 'OK') {
        this.navCtrl.closePopup();
        this.toastr.success(value.message);
        this.setLoggedIn(value.data.user);
        this.navCtrl.goTo('/auth/login', {}, 'root');
        return;
      }
      this.toastr.error('Something went wrong');
    };
    this.requestService.send('copierSignup', user, success);
  }

  async copierForgetPassword(data: any, response: any = null) {
    this.requestService.send('copierForgetPassword', data, response);
  }

  async copierResetPasswordUsingOtp(data: any, response: any = null) {
    this.requestService.send('copierResetPasswordUsingOtp', data, response);
  }
  async copierChangePassword(data: any, response: any = null) {
    this.requestService.send('copierPasswordUpdate', data, response);
  }

  async copierUpdateProfile(data: any, response: any = null) {
    this.requestService.send('copierProfileUpdate', data, response);
  }

  async getUserLogout() {
    const success = (value: any) => {
      if (value.status !== 'OK') {
        this.toastr.error(value.message);
        return;
      }
      this.logout();
    };

    this.requestService.send('UserLogout', {}, success);
  }

}
