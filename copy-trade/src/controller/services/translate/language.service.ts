import { Injectable } from '@angular/core';
import { RequestService } from 'src/controller/utility/handler/request.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  supportLangs = ['en', 'ar'];

  languages = {
      en: {
          name: "English",
          native_name: "English",
          direction: "ltr",
          code: "en"
      },
      ar: {
          name: "Arabic",
          native_name: "اَلْعَرَبِيَّةُ",
          direction: "rtl",
          code: "ar"
      }
  };

  selectedLanguage =   {
    name: 'English (UK)',
    direction: 'ltr',
    code: 'en'
  };

  constructor(public translate: TranslateService, public request: RequestService, public toastr: ToastrService,) {

  }

  get languagesArray() {
    return Object.values(this.languages);
  }

  init() {
    let selectLang: 'en' | 'ar' = 'en';

    const localLanguage = localStorage.getItem('language');
    if(localLanguage != null && this.supportLangs.includes(localLanguage)) {
      selectLang = (localLanguage as ( 'en' | 'ar'));
    }

    this.translate.setDefaultLang(selectLang);
    this.selectedLanguage = (this.languages[selectLang] as any);
    document.body.dir = this.selectedLanguage.direction;
    this.confirmToBackend();

    this.getLanguages();
  }

  confirmToBackend() {
    const success = (value: any) => {
      if (value.status !== 'OK') {
        this.toastr.error(value.desc);
        return;
      }

      localStorage.setItem('language', this.selectedLanguage.code);
    };

    const data = { language : this.selectedLanguage.code };
    this.request.send('UsersChangeLanguage', data, success, null, true);
  }

  getLanguages() {
    const success = (value: any) => {
      if (value.status !== 'OK') {
        this.toastr.error(value.desc);
        return;
      }
      this.languages = value.result.languages;
    };

    this.request.send('SettingsGetLanguages', {}, success, null, true);
  }

  changeLanguage(lang: string = 'en') {
    this.translate.setDefaultLang(lang);
    this.selectedLanguage = (this.languages[(lang as 'en' | 'ar')] as any);
    document.body.dir = this.selectedLanguage.direction;
    this.confirmToBackend();
  }
}
