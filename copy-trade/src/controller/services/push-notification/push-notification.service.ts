import { ActionPerformed, PushNotificationSchema, PushNotifications, Token } from '@capacitor/push-notifications';

import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class PushNotificationService {

  isRegister = false;
  token: Token | null = null;

  constructor(private platform: Platform) { }

  registerPushNotifications() {
    if (this.platform.is('capacitor')) {
      // Request permission to use push notifications
      // iOS will prompt user and return if they granted permission or not
      // Android will just grant without prompting
      PushNotifications.requestPermissions().then((result: any) => {
        if (result.receive === 'granted') {
          // Register with Apple / Google to receive push via APNS/FCM
          PushNotifications.register();
          this.isRegister = true;
        } else {
          // Show some error
        }
      });
    }
  }


  onRegistration(callback: any = null) {
    if (this.platform.is('capacitor')) {
      // On success, we should be able to receive notifications
      PushNotifications.addListener('registration', (token: Token) => {
        this.token = token;
        if (callback) {
          callback(token);
        }
      });
    }
  }

  onRegistrationError(callback: any = null) {
    if (this.platform.is('capacitor')) {
      // Some issue with our setup and push will not work
      PushNotifications.addListener('registrationError', (error: any) => {
        if (callback) {
          callback(error);
        }
      });
    }
  }

  onPushNotificationReceived(callback: any = null) {
    if (this.platform.is('capacitor')) {
      // Show us the notification payload if the app is open on our device
      PushNotifications.addListener('pushNotificationReceived', (notification: PushNotificationSchema) => {
        if (callback) {
          callback(notification);
        }
      });
    }
  }

  onPushNotificationActionPerformed(callback: any = null) {
    if (this.platform.is('capacitor')) {
      // Method called when tapping on a notification
      PushNotifications.addListener('pushNotificationActionPerformed', (notification: ActionPerformed) => {
        if (callback) {
          callback(notification);
        }
      });
    }
  }

}
