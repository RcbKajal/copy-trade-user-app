import { Injectable } from '@angular/core';
import { LocalNotifications } from '@capacitor/local-notifications';
import { Platform } from '@ionic/angular';
import { PushNotificationSchema } from '@capacitor/push-notifications';

@Injectable({
  providedIn: 'root'
})
export class LocalNotificationService {

  constructor(private platform: Platform) {
    if(this.platform.is('capacitor')) {
      this.platform.ready().then(() => {
        LocalNotifications.requestPermissions().then(res => {
          console.log(res);
        });

        LocalNotifications.addListener('localNotificationActionPerformed',
          (payload) => {
            console.log(payload);
          }
        );
      });
    }
  }

  schedule(notification: PushNotificationSchema) {
    LocalNotifications.checkPermissions().then(res => {
      if(res) {
        LocalNotifications.schedule({
          notifications: [
            {
              id: (Math.floor(Math.random() * 100) + 1),
              title: notification.title as string,
              body: notification.body as string,
              schedule: { at: new Date(Date.now() + 1000 * 2) },
              extra: notification,
              // smallIcon: 'https://appcontrol.friendlyfoods.in/favicons/favicon.ico'
            }
          ]
        });
      }
    });
  }
}
