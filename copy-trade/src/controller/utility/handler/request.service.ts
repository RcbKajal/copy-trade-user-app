/* eslint-disable no-underscore-dangle */
/* eslint-disable guard-for-in */

import { HttpClient, HttpHeaders } from '@angular/common/http';

import API from '../../app/data/api';
import { Injectable } from '@angular/core';
import { Request } from './Request';
import { SpinnerService } from '../ui/spinner.service';
import { ToastrService } from '../ui/toastr.service';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  requestor = 'http';
  defaultSpinner!: HTMLIonLoadingElement;

  constructor(
    private http: HttpClient,
    private spinner: SpinnerService,
    private toastr: ToastrService
  ) { }

  async send(requestName: any, data: any = {}, successHandler: any = null, errorHandler = null, backgroundmode = false,
    loaderHandler: any = 'spinner', formData = false) {

    if(typeof API.endpoints[requestName] === 'undefined') {
      this.toastr.error('Endpoint not defined');
      return;
    }

    let loader: any = null;
    const e: any = this;

    const url = () => {
      let tempurl = '';
      if (typeof API.endpoints[requestName].domain == 'undefined') {
        tempurl = API.defaultDomain() + API.endpoints[requestName].url;
      } else {
        tempurl = API.endpoints[requestName].domain + API.endpoints[requestName].url;
      }

      for (const key in data) {
        const variable = '{{' + key + '}}';
        tempurl = tempurl.replace(variable, data[key]);
      }
      return tempurl;
    };


    if (backgroundmode === false) {
      loader = await e[loaderHandler].start();
    }

    const closeLoader = () => {
      if (loaderHandler === 'spinner') {
        if (loader !== null) {
          e[loaderHandler].stop(loader);
        }
      }
    };

    const errorHandlerDefault = (error: any) => {
      if(error?.error?.status == 'FAILED') {
       this.toastr.error(error?.error?.message);
       return;
      }

      if (error?.error?.status) {
        this.toastr.error(error?.error?.desc);
        return;
      }

      this.toastr.error('Something went wrong');
    };

    if (API.endpoints[requestName].requestType === 'post' || API.endpoints[requestName].requestType === 'put') {

      const _url = url();

      if (formData === true) {
        const tempData = new FormData();

        for (const key in data) {
          tempData.append(key, data[key]);
        }

        data = tempData;
      }

      const requestor: HttpClient | any = e[this.requestor];
      const subscriber = requestor[API.endpoints[requestName].requestType](
        _url,
        data,
        {withCredentials: true}
      ).subscribe((value: any) => {
        subscriber.unsubscribe();
        closeLoader();
        if(successHandler !== null) {
          if(value?.status == 'OK') {
            successHandler(value);
          } else {
            this.toastr.error('Internal Server Error');
          }
        }

      }, (error: any) => {
        subscriber.unsubscribe();
        closeLoader();
        if (errorHandler === null) {
          errorHandlerDefault(error);
        }
      }, () => {
        closeLoader();
      });
    }

    if (API.endpoints[requestName].requestType === 'delete') {
      const requestor: HttpClient = e[this.requestor];
      const subscriber = requestor.delete(url(), { body: data }).subscribe(
        (value: any) => {
          subscriber.unsubscribe();
          closeLoader();
          if(value?.status == 'OK') {
            successHandler(value);
          } else {
            this.toastr.error('Internal Server Error');
          }
        },
        (error) => {
          subscriber.unsubscribe();
          closeLoader();

          if (errorHandler === null) {
            errorHandlerDefault(error);
          }
        },
        () => {
          closeLoader();
        }
      );
    }

    if (API.endpoints[requestName].requestType === 'get') {
      const subscriber = e[this.requestor].get(url()).subscribe((value: any) => {
        subscriber.unsubscribe();
        closeLoader();
        if (successHandler !== null) {
          if(value?.status == 'OK') {
            successHandler(value);
          } else {
            this.toastr.error('Internal Server Error');
          }
        }
      }, (error: any) => {
        subscriber.unsubscribe();
        closeLoader();
        if (errorHandler === null) {
          errorHandlerDefault(error);
        }
      }, () => {
        closeLoader();
      });
    }
  }


  create(requestName: any, data: any = {}, successHandler: any = null, errorHandler = null,
    backgroundmode = false, loaderHandler: any = 'spinner', formData = false) {
    const request: Request = new Request(requestName);
    if(backgroundmode) {
      request.slient();
    }

    request.params(data);

    if(formData) {
      request.formData(data);
    } else {
      request.data(data);
    }

    if(successHandler != null) {
      request.onSuccess = successHandler;
    }

    const errorHandlerDefault = (error: any) => {
      if(error?.error?.status == 'FAILED') {
       this.toastr.error(error?.error?.message);
       return;
      }

      if (error?.error?.status) {
        this.toastr.error(error?.error?.desc);
        return;
      }

      this.toastr.error('Something went wrong');
    };

    if(errorHandler != null) {
      request.onError = errorHandler;
    } else {
      request.onError = errorHandlerDefault;
    }

    if(loaderHandler !== 'spinner') {
      request.setSpinnerElement(loaderHandler);
    }

    return request;
  }


  exe(request: Request) {
    if(request.errors.length > 0) {
      this.toastr.error(request.errors.join(', '));
      return;
    }

    let loader: any = null;
    // let spinnerElementParent: HTMLElement | null = null
    if (request.backgroundmode === false) {
      // spinnerElementParent = this.spinnerStart(request);
      if(request.spinner) {
        request.spinner.show = true;
      } else {
        loader = this.spinner.start();
      }
    }

    const closeLoader = () => {
      // this.spinnerStop(request, spinnerElementParent);
      if(request.spinner) {
        request.spinner.show = false;
      }
      else {
        if (loader !== null) {
          this.spinner.stop(loader);
        }
      }
    };

    if(request.onError === undefined) {
      request.onError = (error: any) => {
        if (error.error.status !== undefined && error.error.status) {
          this.toastr.error(error.error.desc);
        }
        else {
          this.toastr.error('Something went wrong');
        }
      };
    }

    const e: any = this;
    const requestor: HttpClient = e[this.requestor];

    if(['post', 'put'].includes(request.method)) {
      const subscriber = requestor[request.method as 'post' | 'put'](request.url, request.body).subscribe((value: any) => {
        subscriber.unsubscribe();
        if(request.onSuccess === undefined) {
          request.onSuccess(value);
          closeLoader();
        }
      }, (error: any) => {
        subscriber.unsubscribe();
        closeLoader();
        request.onError(error);
      }, () => {
        closeLoader();
      });
      return;
    }

    if(['get'].includes(request.method)) {
      const subscriber = requestor[request.method as 'get'](request.url).subscribe((value: any) => {
        subscriber.unsubscribe();
        if(request.onSuccess !== undefined) {
          request.onSuccess(value);
        }
        closeLoader();

      }, (error: any) => {
        subscriber.unsubscribe();
        closeLoader();
        request.onError(error);
      }, () => {
        closeLoader();
      });
      return;
    }

    if(['delete'].includes(request.method)) {
      const subscriber = requestor[request.method as 'delete'](request.url, {body: request.body}).subscribe((value: any) => {
        subscriber.unsubscribe();
        if(request.onSuccess === undefined) {
          request.onSuccess(value);
        }
        closeLoader();
      }, (error: any) => {
        subscriber.unsubscribe();
        closeLoader();
        request.onError(error);
      }, () => {
        closeLoader();
      });
      return;
    }
  }

  //  Private functions

  private spinnerStart(request: Request) {
    const spinnerElementParent = document.createElement('div');
    const spinnerElement = document.createElement('div');
    spinnerElement.style.position = 'absolute';
    spinnerElement.style.top = '0';
    spinnerElement.style.bottom = '0';
    spinnerElement.style.left = '0';
    spinnerElement.style.right = '0';
    spinnerElement.style.display = 'flex';
    spinnerElement.style.justifyContent = 'center';
    spinnerElement.style.alignItems = 'center';

    const spinner = document.createElement('img');
    spinner.src = 'assets/images/icons/loading-icon.gif';
    spinner.width = 30;

    spinnerElement.append(spinner);
    spinnerElementParent.append(spinnerElement);
    if(request.spinnerElementParent != null) {
      this.copyStyleOfHtml(request.spinnerElementParent, spinnerElementParent);
      request.spinnerElementParent.style.display = 'none';

      request.spinnerElementParent.replaceWith(spinnerElementParent);
      return spinnerElementParent;
    }

    this.spinner.start().then(defaultSpinner => {
      this.defaultSpinner = defaultSpinner;
    });

    return null;
  }

  private spinnerStop(request: Request, spinnerElementParent: HTMLElement | null, ) {
    if(spinnerElementParent != null && request.spinnerElementParent != null) {
      spinnerElementParent.replaceWith(request.spinnerElementParent);
      spinnerElementParent.remove();
    } else {
      this.spinner.stop(this.defaultSpinner);
    }
  }

  private copyStyleOfHtml(org: HTMLElement, dup: HTMLElement) {
    // 👇️ Get computed styles of original element
    const styles = window.getComputedStyle(org);

    let cssText = styles.cssText;

    if (!cssText) {
      cssText = Array.from(styles).reduce((str, property) => `${str}${property}:${styles.getPropertyValue(property)};`, '');
    }

    // 👇️ Assign css styles to element
    dup.style.cssText = cssText;
  }
}
