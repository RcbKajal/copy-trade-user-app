import { Injectable, ViewChild } from '@angular/core';

import { DatatimePickerComponent } from './datatime-picker.component';
import { NavigationRouteService } from '../../handler/navigation-route.service';

@Injectable({
  providedIn: 'root'
})
export class DatatimePickerService {

  constructor(private navCtrl: NavigationRouteService) { }

  open() {
    this.navCtrl.popup(DatatimePickerComponent, null, null, 'content-fit-popup').then(modal => {
      modal.onDidDismiss().then(data => {;
      });
    })
  }

}
