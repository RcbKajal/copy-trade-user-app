import { Component, OnInit, ViewChild } from '@angular/core';

import { NavigationRouteService } from '../../handler/navigation-route.service';

@Component({
  selector: 'app-datatime-picker',
  templateUrl: './datatime-picker.component.html',
  styleUrls: ['./datatime-picker.component.scss']
})
export class DatatimePickerComponent implements OnInit {

  date:any = new Date().toISOString();

  constructor(private navCtrl: NavigationRouteService) { }

  ngOnInit(): void {

  }

  onConfirm() {
    this.navCtrl.closePopup(this.date)
  }
}
