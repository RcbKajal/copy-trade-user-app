import { Injectable, NgZone } from '@angular/core';

import { Geolocation } from '@capacitor/geolocation';

// import { NativeGeocoder, NativeGeocoderOptions, NativeGeocoderResult } from '@ionic-native/native-geocoder/ngx';



// import { Geolocation } from '@ionic-native/geolocation/ngx';


declare let google: any;

@Injectable({
  providedIn: 'root'
})
export class GeoLocationService {
  address: string = '';
  autocomplete = { input: '' };
  autocompleteItems: Array<any> = [];
  googleAutocomplete: any;
  map: any;
  latitude = "42.38886033139711";
  longitude = "-76.45501825757424";
  googleMap: any = google;
  constructor(
    // private nativeGeocoder: NativeGeocoder,
    public zone: NgZone
  ) {
    this.googleAutocomplete = new google.maps.places.AutocompleteService();
  }


  initDirectionsRenderer() {
    return new google.maps.DirectionsRenderer();
  }

  initMap(latLng: any, mapElement: any) {
    const mapOptions = {
      center: latLng,
      zoom: 5,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      streetViewControl: false,
      mapTypeControl: false,
      disableDefaultUI: true,
    };
    return new google.maps.Map(mapElement, mapOptions);
  }

  changeCenterMap(map: any, lat: any, lng: any) {
    const latLng = new google.maps.LatLng(lat, lng);
    map.panTo(latLng);
  }

  addMarker(map: any, lat: any, lng: any, draggable = false, listeners: any = null, customIcon: any = null, infoWindow: any = null) {
    const position = { lat, lng };
    const marker = new google.maps.Marker({
      position,
      map,
      draggable,
      animation: google.maps.Animation.DROP,
      icon: customIcon,
    });

    if (listeners != null) {
      for (let listener in listeners) {
        google.maps.event.addListener(marker, listener, function () {
          listeners[listener](marker);
        });
      }
    }

    if (infoWindow) {
      let infowindow = new google.maps.InfoWindow();
      const div = document.createElement('div');
      div.append(infoWindow.location.nativeElement);

      google.maps.event.addListener(marker, 'click', (function (marker, i) {
        return function () {
          infowindow.setContent(div);
          infowindow.open(map, marker);
        }
      })(marker));
    }

    return marker;
  }

  setBound(map: any, latlngArray: Array<any>) {
    let bounds = new google.maps.LatLngBounds();

    for (let latlng of latlngArray) {
      bounds.extend(new google.maps.LatLng(latlng.lat, latlng.lng))
    }

    map.fitBounds(bounds);
  }

  addMultiMarker(map: any, locations: Array<any>, customIcon: any = undefined) {
    let infowindow = new google.maps.InfoWindow();

    let markers = [], marker, i;
    let bounds = new google.maps.LatLngBounds();
    for (i = 0; i < locations.length; i++) {

      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
        map: map,
        icon: customIcon,
      });

      bounds.extend(marker.getPosition());

      if (locations[i].infoWindow) {
        const div = document.createElement('div');
        div.append(locations[i].infoWindow.location.nativeElement);

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
          return function () {
            infowindow.setContent(div);
            infowindow.open(map, marker);
          }
        })(marker, i));
      }


      markers.push({ marker, infoWindow: locations[i].infoWindow });
    }
    map.fitBounds(bounds);

    return markers;
  }

  loadMap(mapElement: any) {

    return new Promise((resolve, reject) => {
      const latLng = new google.maps.LatLng(this.latitude, this.longitude);

      // Geolocation.getCurrentPosition().then((resp: any) => {
      //   const latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      //   resolve(this.initMap(latLng, mapElement));

      // }).catch((error: any) => {
      resolve(this.initMap(latLng, mapElement));
      // });;
    });

  }

  getCurrectLocation() {
    return new Promise((resolve, reject) => {
      Geolocation.getCurrentPosition().then((resp: any) => {
        resolve({ lat: resp.coords.latitude, lng: resp.coords.longitude });
      }).catch((error: any) => {
        reject(false);
        console.log('Error getting location', error);
      });
    });
  }

  startWatch() {
    const options = {
      enableHighAccuracy: false,
      timeout: 30000,
      maximumAge: 10000
    };

    const watch = Geolocation.watchPosition(options, (data: any) => {

    });
  }

  decodePath(path: string) {
    return google.maps.geometry.encoding.decodePath(path)
  }

  // reverseGeocode(lat: any, lng: any) {
  //   return new Promise((resolve, reject) => {
  //     const options: NativeGeocoderOptions = {
  //       useLocale: true,
  //       maxResults: 1
  //     };
  //     this.nativeGeocoder.reverseGeocode(lat, lng, options).then((result: NativeGeocoderResult[]) => {
  //       resolve(result);
  //     }).catch((error: any) => {
  //       reject(error);
  //     });
  //   });
  // }

  // forwardGeocode(str: any) {
  //   return new Promise((resolve, reject) => {
  //     const options: NativeGeocoderOptions = {
  //       useLocale: true,
  //       maxResults: 1
  //     };
  //     this.nativeGeocoder.forwardGeocode(str, options).then((result: NativeGeocoderResult[]) => {
  //       resolve(result);
  //     }).catch((error: any) => {
  //       reject(error);
  //     });
  //   });
  // }

  clearAutocomplete() {
    this.autocompleteItems = [];
    // this.autocomplete.input = '';
  }

  updateSearchResults() {
    if (this.autocomplete.input === '') {
      this.autocompleteItems = [];
      return;
    }
    this.googleAutocomplete.getPlacePredictions({ input: this.autocomplete.input },
      (predictions: any, status: any) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          predictions.forEach((prediction: any) => {
            this.autocompleteItems.push(prediction);
          });
        });
      });
  }

  getReverseGeocodingData(lat: any, lng: any) {
    return new Promise((resolve, reject) => {
      const latlng = new google.maps.LatLng(lat, lng);
      const geocoder = new google.maps.Geocoder();
      geocoder.geocode({ latLng: latlng }, (results: any, status: any) => {
        if (status !== google.maps.GeocoderStatus.OK) {
          resolve(false);
        }

        if (status === google.maps.GeocoderStatus.OK) {
          if (results.length > 0) {
            const fetchAddress = (_results: any) => {
              for (const result of _results) {
                if (result.types.includes('premise') ||
                  result.types.includes('street_address') ||
                  result.types.includes('route') ||
                  result.types.includes('political') ||
                  result.types.includes('sublocality') ||
                  result.types.includes('sublocality_level_1') ||
                  result.types.includes('sublocality_level_2')) {
                  return (result.formatted_address);;
                }
              }
              return (results[0].formatted_address);
            };
            resolve(fetchAddress(results));
          } else {
            resolve(false);
          }
        }
      });
    });
  }

  getForwardGeocodingData(address: any) {
    return new Promise((resolve, reject) => {
      const geocoder = new google.maps.Geocoder();
      geocoder.geocode({ address }, (results: any, status: any) => {
        if (status === google.maps.GeocoderStatus.OK) {
          if (results.length > 0) {
            resolve(results[0].geometry.location);
          } else {
            resolve(false);
          }
        } else {
          resolve(false);
        }
      });
    });
  }

  getPolyline(obj: any) {

    if (obj?.path?.encoded === true) {
      obj.path = this.decodePath(obj?.path?.code);
    }

    return new google.maps.Polyline(obj);
  }

  getPolygon(obj: any) {
    return new google.maps.Polygon(obj);
  }
}
