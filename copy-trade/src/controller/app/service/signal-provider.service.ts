import * as moment from 'moment';

import { Injectable } from '@angular/core';
import { InstrumentModel } from '../models/Instrument.model';
import { RequestService } from 'src/controller/utility/handler/request.service';

@Injectable({
  providedIn: 'root'
})
export class SignalProviderService {

  instruments: InstrumentModel[] = [];

  constructor(public request: RequestService,) { }

  async getInstruments() {
    const success = (value: any) => {
      this.instruments = InstrumentModel.createFromArray(value.data);
    };

    this.request.send('getInstruments', {}, success);
  }

  async searchInstruments(searchString: string): Promise<InstrumentModel[]> {
    return new Promise((resolve, reject) => {
      resolve(
        this.instruments.filter(item => {
          return item.exchange == 'NSE' && item?.name?.toLowerCase()?.includes(searchString)
        })
      )
    });
  }

  async getQuote(selectedInstrument: InstrumentModel, success: (result: any) => void) {
    this.request.send('getQuote', selectedInstrument, success);
  }

  async executeOrder(data: any, success: (result: any) => void) {
    this.request.send('executeOrder', data, success);
  }

  async getOpenOrder(success: (result: any) => void) {
    this.request.send('openOrders', {}, success);
  }
}
