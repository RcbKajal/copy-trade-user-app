import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { Injectable } from '@angular/core';
import { RequestService } from 'src/controller/utility/handler/request.service';
import { ToastrService } from 'src/controller/utility/ui/toastr.service';
import { Token } from '@capacitor/push-notifications';
import { UserModel } from '../models/User.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  users: Array<UserModel> = [];

  constructor( private toastr: ToastrService, private request: RequestService, public auth: AuthenticationService) { }

  registerToken(token: Token) {
    this.request.send('registerToken', {token});
  }

  getAllProvider(data: any, backgroundMode = false, response: any = null) {
    this.request.send('getAllProvider', data, response, null, backgroundMode);
  }

  follow(data: any, backgroundMode = false, response: any = null) {
    this.request.send('followSignalProvider', data, response, null, backgroundMode);
  }

  getFollowedSignalProviders(response: any = null) {
    this.request.send('followedSignalProvider', {}, response);
  }

  updatePassword(data: any) {
    const success = (value: any) => {
      if (value.status !== 'OK') {
        this.toastr.error(value.message);
        return;
      }

      this.toastr.success(value.message);
    };

    this.request.send('updatePassword', data, success);
  }

  updateUserProfile(data: any, response: any = null) {
    const success = (value: any) => {
      if (value.status !== 'OK') {
        this.toastr.error(value.message);
        return;
      }

      if (response != null) {
        response(value);
      }
    };

    this.request.send('myProfileUpdate', data, success, null, false, undefined, false);
  }


}
