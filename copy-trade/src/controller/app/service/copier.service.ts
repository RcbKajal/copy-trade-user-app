import { Injectable } from '@angular/core';
import { RequestService } from 'src/controller/utility/handler/request.service';

@Injectable({
  providedIn: 'root'
})
export class CopierService {

  signals: any = [];
  providers: any = [];

  constructor(public request: RequestService) { }

  async getSignals() {
    const success = (value: any) => {
      this.signals = value.data;
    };

    this.request.send('getSignals', {}, success);
  }

  async getProviders(onSuccess: any = null) {
    const success = (value: any) => {
      this.providers = value.data;
      if(onSuccess) {
        onSuccess(this.providers);
      }
    };

    this.request.send('getProviders', {}, success);
  }
}
