import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { Injectable } from '@angular/core';
import { NavigationRouteService } from 'src/controller/utility/handler/navigation-route.service';
import { RequestService } from 'src/controller/utility/handler/request.service';
import { ToastrService } from 'src/controller/utility/ui/toastr.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  isSlideOpen = false;
  constructor(
    public request: RequestService,
    public toastr: ToastrService,
    public navCtrl: NavigationRouteService,
    public auth: AuthenticationService,
  ) { }

  initHome() {
    this.auth.loadCurrentUser();
  }

  slideToggle() {
    this.isSlideOpen = !this.isSlideOpen;
  }
}
