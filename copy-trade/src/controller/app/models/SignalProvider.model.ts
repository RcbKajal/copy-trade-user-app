import { Model } from './Model';
import { environment } from 'src/environments/environment';

export class SignalProviderModel extends Model {

  override className = "SignalProvider";

  id= null;
  firstName= null;
  lastName= null;
  email= null;
  mobile= null;
  image= null;
  roles= [
    {
      id: 1,
      name: 'copier'
    }
  ];

  address= null;
  state= null;
  townOrCity= null;
  company= null;
  postalCode= null;
  phoneNumber= null;
  dob= null;
  gender= null;
  country= null;

  get getProfilePic() {
    if (!this.image) {
      return '/assets/images/user/dummy-user.png';
    }

    return environment.ASSETS_URL + this.image;
  }
}
