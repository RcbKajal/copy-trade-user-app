import { environment } from 'src/environments/environment';
import { Model } from './Model';

export class UserModel extends Model {

  override className = 'User';

  id = null;
  mobile = null;
  email = null;
  password = null;
  token = null;
  refreshToken = null;
  createdAt = null;
  deletedAt = null;
  updatedAt = null;

  profile = {
    id: null,
    firstName: null,
    lastName: null,
    email: null,
    mobile: null,
    image: null,
    roles: [
      {
        id: 1,
        name: 'copier'
      }
    ],
    tradeAccount: {
      id: null,
      initialAmount: null,
      currentBalance: null,
    },

    address: null,
    state: null,
    townOrCity: null,
    company: null,
    postalCode: null,
    phoneNumber: null,
    dob: null,
    gender: null,
    country: null
  }

  get getId() {
    return this.id;
  }

  get getProfilePic() {
    if (!this.profile.image) {
      return '/assets/images/user/dummy-user.png';
    }

    return environment.ASSETS_URL + this.profile.image;
  }

  get isCopier() {
    for(let role of this.profile.roles) {
      if(role.name == 'copier') {
        return true;
      }
    }
    return false;
  }

  get isSignalProvider() {
    for(let role of this.profile.roles) {
      if(role.name == 'signalProvider') {
        return true;
      }
    }
    return false;
  }

}
