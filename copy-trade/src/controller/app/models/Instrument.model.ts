import { Model } from "./Model";

export class InstrumentModel extends Model {
  instrument_token!: string;
  exchange_token!: string;
  tradingsymbol!: string;
  name!: string;
  last_price!: number;
  expiry!: Date;
  strike!: number;
  tick_size!: number;
  lot_size!: number;
  instrument_type!: string;
  segment!: string;
  exchange!: string;
}
