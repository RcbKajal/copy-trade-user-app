/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/naming-convention */

import { Constants } from './constant';
import { Injectable } from '@angular/core';
import { StoreService } from '../../database/store.service';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  default = {
    disclaimer: false,
    token: null,
    hasLoggedIn: false,
    user_id: null,
    user: null,
    allCategoriesWithProduct: [
      { id: 1, categoryName: 'wine', categorySlug: 'wine-1' },
      { id: 2, categoryName: 'spirits', categorySlug: 'spirits-2' },
      { id: 3, categoryName: 'collers', categorySlug: 'collers-3' },
      { id: 4, categoryName: 'Beer&Cider', categorySlug: 'beercider-4' }
    ]
  };

  get baseDisclaimer() {
    return StoreService.get(Constants.DISCLAIMER) ?? this.default.disclaimer;
  };

  set baseDisclaimer(val) {
    StoreService.set(Constants.DISCLAIMER, val);
  }

  get userID() {
    return StoreService.get(Constants.USER_ID) ?? this.default.user_id;
  }

  set userID(val) {
    StoreService.set(Constants.USER_ID, val);
  }

  get hasLoggedIn() {
    return StoreService.get(Constants.HAS_LOGGED_IN) ?? this.default.hasLoggedIn;
  }

  set hasLoggedIn(val) {
    StoreService.set(Constants.HAS_LOGGED_IN, val);
  }

  get token() {
    return StoreService.get(Constants.TOKEN) ?? this.default.token;
  }

  set token(val) {
    StoreService.set(Constants.TOKEN, val);
  }

  get user() {
    return StoreService.get(Constants.USER) ?? this.default.token;
  }

  set user(val) {
    StoreService.set(Constants.USER, val);
  }

  // large data
  get allCategoriesWithProduct() {
    return StoreService.get(Constants.AllCategoriesWithProduct) ?? this.default.allCategoriesWithProduct;
  }

  set allCategoriesWithProduct(val) {
    StoreService.set(Constants.AllCategoriesWithProduct, val);
  }


  get primaryAddress() {
    return StoreService.get(Constants.USER_ADDRESS) ?? null;
  }

  set primaryAddress(val) {
    StoreService.set(Constants.USER_ADDRESS, val);
  }

}
