import { environment } from 'src/environments/environment';

const api: any = {
  endpoints: {

    // Auth Process
        //copier
        copierLogin: {
          requestType: 'post',
          url: '/copier/auth/login'
        },
        copierSignup: {
          requestType: 'post',
          url: '/copier/auth/register',
        },
        copierSignupSendOtp: {
          requestType: 'post',
          url: '/copier/auth/register-send-otp',
        },
        copierForgetPassword: {
          requestType: 'post',
          url: '/copier/auth/forgot-password',
        },
        copierResetPasswordUsingOtp: {
          requestType: 'post',
          url: '/copier/auth/reset-password',
        },

        //signal provider
        signalProviderLogin: {
          requestType: 'post',
          url: '/signalProvider/auth/login'
        },
        signalProviderSignupSendOtp: {
          requestType: 'post',
          url: '/signalProvider/auth/register-send-otp',
        },
        signalProviderSignup: {
          requestType: 'post',
          url: '/signalProvider/auth/register',
        },
        signalProviderForgetPassword: {
          requestType: 'post',
          url: '/signalProvider/auth/forgot-password',
        },
        signalProviderResetPasswordUsingOtp: {
          requestType: 'post',
          url: '/signalProvider/auth/reset-password',
        },

    // User
          // Copier
          copierProfile: {
            requestType: 'get',
            url: '/copier/user/me',
          },
          copierProfileUpdate: {
            requestType: 'put',
            url: '/copier/user/update',
          },
          copierPasswordUpdate: {
            requestType: 'put',
            url: '/copier/user/change-password'
          },
          registerToken: {
            requestType: 'put',
            url: '/copier/user/register-token'
          },
          getAllProvider: {
            requestType: 'get',
            url: '/copier/user/all-signal-provider',
          },
          followSignalProvider: {
            requestType: 'put',
            url: '/copier/user/follow-signal-provider',
          },
          followedSignalProvider: {
            requestType: 'get',
            url: '/copier/user/followed-signal-provider',
          },

          // Signal Provider
          signalProviderProfile: {
            requestType: 'get',
            url: '/signalProvider/user/me',
          },
          signalProviderUpdate: {
            requestType: 'put',
            url: '/signalProvider/user/update',
          },
          getAllFollowers: {
            requestType: 'get',
            url: '/signalProvider/user/all-followers',
          },
          signalProviderProfileUpdate: {
            requestType: 'put',
            url: '/signalProvider/me',
          },
          signalProviderPasswordUpdate: {
            requestType: 'put',
            url: '/signalProvider/user/change-password'
          },

    // stock
    getInstruments: {
      requestType: 'get',
      url: '/stock/instruments'
    },
    getQuote: {
      requestType: 'post',
      url: '/stock/get-quotes'
    },
    executeOrder: {
      requestType: 'post',
      url: '/stock/execute-order'
    },
    openOrders:  {
      requestType: 'get',
      url: '/stock/open-orders'
    },

    // Copier Signals
    getSignals: {
      requestType: 'get',
      url: '/copier/signals'
    },
    getProviders: {
      requestType: 'get',
      url: '/copier/signals/providers'
    }
  },
  defaultDomain: () => environment.API_URL,
};

export default api;
