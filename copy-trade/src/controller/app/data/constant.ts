/* eslint-disable @typescript-eslint/naming-convention */
export const Constants = {
    HAS_LOGGED_IN: 'hasLoggedIn',
    DISCLAIMER: 'disclaimer',
    TOKEN: 'token',
    USER_ID: 'user_id',
    USER: 'User',
    AllCategoriesWithProduct: 'AllCategoriesWithProduct',
    USER_ADDRESS: 'userAddress',
};
