import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from 'src/controller/app/service/app.service';
import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { NavigationRouteService } from 'src/controller/utility/handler/navigation-route.service';
import { ToastrService } from 'src/controller/utility/ui/toastr.service';
import { ResetPage } from './reset/reset.page';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  form = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required])
  });
  constructor(private navCtrl: NavigationRouteService, public appService: AppService, public auth: AuthenticationService, private toastr: ToastrService) { }

  ngOnInit() {
  }

  forgotPassword() {
    this.form.markAllAsTouched();
    if (this.form.invalid) {
      return;
    }
    const data = this.form.value;

    const success = (value: any) => {
      this.navCtrl.popup(ResetPage, { data }).then(modal => {
        modal.onDidDismiss().then(value => {
          if (value.data.navigateToLogin) {
            this.gotoLogin();
          }
        })
      });
      this.toastr.success(value.message);
      return;
    };

    this.auth.copierForgetPassword(data, success);
  }

  gotoLogin() {
    this.navCtrl.goTo('/auth/login', {}, 'back');
  }
}
