import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from 'src/controller/app/service/app.service';
import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { NavigationRouteService } from 'src/controller/services/navigation-route/navigation-route.service';
import { ToastrService } from 'src/controller/utility/ui/toastr.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.page.html',
  styleUrls: ['./reset.page.scss'],
})
export class ResetPage implements OnInit {

  form = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [Validators.required]),
    emailOtp: new FormControl('', [Validators.required])
  });

  constructor(private navCtrl: NavigationRouteService, public appService: AppService, public authService: AuthenticationService, private toastr: ToastrService) {}

  ngOnInit(): void {
    if (this.navCtrl.navigationData.data !== null) {
      const user = this.navCtrl.navigationData.data;
      this.form.patchValue({
        email: user.email
      });
    } else {
      this.navCtrl.closePopup();
    }
  }

  resetPassword() {
    this.form.markAllAsTouched();
    if (this.form.invalid) {
      return;
    }
    const data = this.form.value;

    const success = (value: any) => {
      this.toastr.success(value.message);
      this.gotoLogin();
    }
    this.authService.copierResetPasswordUsingOtp(data, success);
  }

  gotoLogin() {
    this.navCtrl.closePopup({navigateToLogin: true});
  }
}
