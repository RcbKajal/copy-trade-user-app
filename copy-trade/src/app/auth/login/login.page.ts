import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { NavigationRouteService } from 'src/controller/utility/handler/navigation-route.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  password = 'password';

  form = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [Validators.required]),
  });
  constructor(public navCtrl: NavigationRouteService, private authService: AuthenticationService) { }


  ngOnInit() {
  }
  signIn() {
    this.form.markAllAsTouched();
    if (this.form.invalid) {
      return;
    }
    const data = this.form.value;
    this.authService.copierLogin(data);

  }
  forgot() {
    this.navCtrl.goTo('/auth/forgot-password', {}, 'forward');
  }

  register() {
    this.navCtrl.goTo('/auth/register', {}, 'root');
  }
}
