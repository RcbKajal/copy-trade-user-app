import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppService } from 'src/controller/app/service/app.service';
import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { NavigationRouteService } from 'src/controller/utility/handler/navigation-route.service';

@Component({
  selector: 'app-register-otp',
  templateUrl: './register-otp.component.html',
  styleUrls: ['./register-otp.component.scss']
})
export class RegisterOtpComponent implements OnInit {

  form = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    // initialAmount: new FormControl('10000', [Validators.required]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    // mobile: new FormControl(''),
    emailOtp: new FormControl('', [Validators.required])
  });

  constructor(private navCtrl: NavigationRouteService, public appService: AppService, public auth: AuthenticationService, public authService: AuthenticationService) { }

  ngOnInit(): void {

    if (this.navCtrl.navigationData.data !== null) {
      const user = this.navCtrl.navigationData.data;
      console.log(user,'register email otp ');
      this.form.patchValue({
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        password: user.password,
        // initialAmount: user.initialAmount,
        emailOtp: user.emailOtp
        // mobile: user.mobile
      });
    } else {
      this.navCtrl.closePopup();
    }
  }

  cancel() {
    this.navCtrl.closePopup();
  }

  saveOtpCode() {
    this.form.markAllAsTouched();
    if (this.form.invalid) {
      return;
    }
    const data = this.form.value;
    this.authService.copierSignup(data);
  }
}
