import * as moment from 'moment';

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { CustomValidators } from 'src/controller/function/CustomValidators';
import { DatatimePickerService } from 'src/controller/utility/ui/datatime-picker/datatime-picker.service';
import { NavigationRouteService } from 'src/controller/utility/handler/navigation-route.service';
import { ToastrService } from 'src/controller/utility/ui/toastr.service';
import { RegisterOtpComponent } from './register-otp/register-otp.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  password = 'password';

  form = new FormGroup({
    firstName: new FormControl('', [
      Validators.required,
      Validators.pattern('[a-zA-Z ]*'),
    ]),
    lastName: new FormControl('', [
      Validators.required,
      Validators.pattern('[a-zA-Z ]*'),
    ]),
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [
      Validators.minLength(8),
      Validators.required,
      CustomValidators.matchValidator('confirmPassword', true),
    ]),
    confirmPassword: new FormControl('', [
      Validators.required,
      CustomValidators.matchValidator('password'),
    ]),
    
  });

  constructor(
    public navCtrl: NavigationRouteService,
    public toastr: ToastrService,
    private authService: AuthenticationService,
    private datePickerService: DatatimePickerService
  ) { }
  ngOnInit() {
  }
  toggleEye() {
    if (this.password === 'password') {
      this.password = 'text';
    } else {
      this.password = 'password';
    }
  }

  register() {
 this.form.markAllAsTouched();
    if (this.form.invalid) {
      return;
    }
    const formData = this.form.value;
    const data: any = {};
    data.email = formData.email;
    // data.mobile = formData.mobile;
    data.firstName = formData.firstName;
    data.lastName = formData.lastName;
    data.password = formData.password;
    // data.initialAmount = formData.initialAmount;
    const res = (value: any) => {
        this.navCtrl.popup(RegisterOtpComponent, { data });
    };

    this.authService.copierSignupSendOtp(data, res);
    // var years = moment().diff(this.form.value.dob, 'years');

    // if (years < 19) {
    //   this.toastr.error('You must be atleast 19 years old.');
    //   return;
    // }

   
  }

  login() {
    this.navCtrl.back();
  }

  datepickerCall() {
    this.datePickerService.open()
  }
}
