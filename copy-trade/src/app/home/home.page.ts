import { Component, OnInit } from '@angular/core';
import { PushNotificationSchema, Token } from '@capacitor/push-notifications';

import { CopierService } from 'src/controller/app/service/copier.service';
import { LocalNotificationService } from 'src/controller/services/push-notification/local-notification.service';
import { PushNotificationService } from 'src/controller/services/push-notification/push-notification.service';
import { UserService } from 'src/controller/app/service/user.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  constructor(private pushNotificationService: PushNotificationService, private userService: UserService, public copierService: CopierService, private localNotificationService: LocalNotificationService) {}

  ngOnInit() {

    this.pushNotificationService.onRegistration((token: Token) => {
      this.userService.registerToken(token);
    });

    this.pushNotificationService.onPushNotificationReceived((notification: PushNotificationSchema) => {
      this.copierService.getSignals();
      this.localNotificationService.schedule(notification)
    });

    this.pushNotificationService.registerPushNotifications();
  }

}
