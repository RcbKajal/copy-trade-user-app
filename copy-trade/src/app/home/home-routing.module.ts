import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      {
        path: '',
        redirectTo: 'signal',
        pathMatch: 'full'
      },
      {
        path: 'signal',
        loadChildren: () =>
          import('./pages/signal/signal.module').then(
            (m) => m.SignalPageModule
          ),
      },
      {
        path: 'followers',
        loadChildren: () =>
          import('./pages/followers/followers.module').then(
            (m) => m.FollowersPageModule
          ),
      },
      {
        path: 'followed',
        loadChildren: () =>
          import('./pages/followed/followed.module').then(
            (m) => m.FollowedPageModule
          ),
      },
      {
        path: 'setting',
        loadChildren: () =>
          import('./pages/setting/setting.module').then((m) => m.SettingPageModule),
      },
     
    ],
  },
 

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule { }
