import { RouterModule, Routes } from '@angular/router';

import { FollowersPage } from './followers.page';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: FollowersPage
  },
  {
    path: 'new-provider/:id',
    loadChildren: () => import('../../new-provider/new-provider.module').then( m => m.NewProviderPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class FollowersPageRoutingModule {}
