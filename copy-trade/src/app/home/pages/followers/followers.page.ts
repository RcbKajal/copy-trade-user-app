import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { NavigationRouteService } from 'src/controller/utility/handler/navigation-route.service';
import { SignalProviderModel } from 'src/controller/app/models/SignalProvider.model';
import { UserService } from 'src/controller/app/service/user.service';

@Component({
  selector: 'app-followers',
  templateUrl: './followers.page.html',
  styleUrls: ['./followers.page.scss'],
})
export class FollowersPage implements OnInit {

  signalProviders: Array<any> = [];
  follow = true;

  constructor(
    public auth: AuthenticationService,
    private userService: UserService,
    public navCtrl: NavigationRouteService,
    private user: UserService
  ) { }

  ngOnInit() {
    this.getAllSignalProviders();
  }

  getAllSignalProviders() {
    const res = (value: any) => {
      this.signalProviders = SignalProviderModel.createFromArray(value.data);
    };
    this.user.getAllProvider({}, true, res);
  }


  gotoProviderDetail(data: any) {
    this.navCtrl.goTo('/home/followers/new-provider/' + data.id, { data }, 'root');
  }

  startFollow(signalProvider: any) {
    this.userService.follow({ id: signalProvider?.id }, false, () => {
      this.follow = false;
    });
  }
}
