import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FollowersPageRoutingModule } from './followers-routing.module';

import { FollowersPage } from './followers.page';
import { DirectiveModule } from 'src/controller/directive/directive.module';
import { HeaderPageModule } from '../../header/header.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DirectiveModule,
    FollowersPageRoutingModule,
    HeaderPageModule
  ],
  declarations: [FollowersPage]
})
export class FollowersPageModule {}
