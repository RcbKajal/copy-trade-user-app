import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HeaderPageModule } from '../../header/header.module';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { SignalPage } from './signal.page';
import { SignalPageRoutingModule } from './signal-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SignalPageRoutingModule,
    HeaderPageModule
  ],
  declarations: [SignalPage]
})
export class SignalPageModule {}
