import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { CopierService } from 'src/controller/app/service/copier.service';
import { NavigationRouteService } from 'src/controller/utility/handler/navigation-route.service';

@Component({
  selector: 'app-signal',
  templateUrl: './signal.page.html',
  styleUrls: ['./signal.page.scss'],
})
export class SignalPage implements OnInit {
  constructor(
    public navCtrl: NavigationRouteService,
    public copierService: CopierService
  ) {}

  ngOnInit(): void {
    this.getSignals();
  }


  getSignals() {
    this.copierService.getSignals();
  }
}
