import { CommonModule } from '@angular/common';
import { DirectiveModule } from 'src/controller/directive/directive.module';
import { FollowedPage } from './followed.page';
import { FollowedPageRoutingModule } from './followed-routing.module';
import { FormsModule } from '@angular/forms';
import { HeaderPageModule } from '../../header/header.module';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DirectiveModule,
    FollowedPageRoutingModule,
    HeaderPageModule
  ],
  declarations: [FollowedPage]
})
export class FollowedPageModule {}
