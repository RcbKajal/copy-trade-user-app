import { Component, OnInit } from '@angular/core';
import { SignalProviderModel } from 'src/controller/app/models/SignalProvider.model';
import { UserService } from 'src/controller/app/service/user.service';
import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { NavigationRouteService } from 'src/controller/utility/handler/navigation-route.service';

@Component({
  selector: 'app-followed',
  templateUrl: './followed.page.html',
  styleUrls: ['./followed.page.scss'],
})
export class FollowedPage implements OnInit {

  signalProviders: Array<any> = [];
  follow = true;

  constructor(public auth: AuthenticationService, public navCtrl: NavigationRouteService, private user: UserService) {}

  ngOnInit() {
    this.getAllSignalProviders();
  }

  getAllSignalProviders() {
    const res = (value: any) => {
      this.signalProviders = SignalProviderModel.createFromArray(value.data);
    };
    this.user.getFollowedSignalProviders(res);
  }

  logout(){
    localStorage.clear();
    this.navCtrl.goTo('/auth/login');
  }

  gotoProviderDetail(data: any) {
    this.navCtrl.goTo('/home/followed/new-provider/' + data.id, { data }, 'root');
  }
}
