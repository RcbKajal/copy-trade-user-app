import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { HeaderPageModule } from '../../header/header.module';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { SettingPage } from './setting.page';
import { SettingPageRoutingModule } from './setting-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SettingPageRoutingModule,
    HeaderPageModule
  ],
  declarations: [SettingPage]
})
export class SettingPageModule {}
