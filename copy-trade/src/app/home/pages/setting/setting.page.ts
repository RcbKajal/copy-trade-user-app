import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { NavigationRouteService } from 'src/controller/utility/handler/navigation-route.service';
import { StateService } from 'src/controller/app/data/state.service';
import { ToastrService } from 'src/controller/utility/ui/toastr.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {

  form = new FormGroup({
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    mobile: new FormControl('', [Validators.required]),
  });

  constructor( private navCtrl: NavigationRouteService, public auth: AuthenticationService, public toastr: ToastrService, public stateService: StateService) {}

  ngOnInit() {
    this.form.patchValue({
      firstName: this.stateService.user?.profile.firstName,
      lastName: this.stateService.user?.profile.lastName,
      email: this.stateService.user?.profile.email,
      mobile: this.stateService.user?.profile.mobile,
    });
  }

  updateProfile() {
    this.form.markAllAsTouched();
    if (this.form.invalid) {
      return;
    }
    const data = this.form.value;

    const success = (value: any) => {
      this.auth.getUserProfile();
      this.toastr.success(value.message);
    };
    this.auth.copierUpdateProfile(data, success);
  }



  logout() {
    localStorage.clear();
    this.navCtrl.goTo('/auth/login');
  }

  changePassword() {
    this.navCtrl.goTo('/home/setting/change-password');
  }
}
