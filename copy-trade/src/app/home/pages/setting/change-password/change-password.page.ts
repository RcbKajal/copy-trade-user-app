import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StateService } from 'src/controller/app/data/state.service';
import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { CustomValidators } from 'src/controller/function/CustomValidators';
import { NavigationRouteService } from 'src/controller/utility/handler/navigation-route.service';
import { ToastrService } from 'src/controller/utility/ui/toastr.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  form = new FormGroup({
    oldPassword: new FormControl('', [Validators.required]),
    newPassword: new FormControl('', [Validators.minLength(8), Validators.required, CustomValidators.matchValidator('confirmPassword', true)]),
    confirmPassword: new FormControl('', [Validators.required, CustomValidators.matchValidator('newPassword')])
  });

  constructor(private navCtrl: NavigationRouteService,
     public auth: AuthenticationService,
     public toastr: ToastrService,
      public stateService: StateService) {}

  ngOnInit() {

  }


  updateNewPassword() {
    this.form.markAllAsTouched();
    if (this.form.invalid) {
      return;
    }
    const data = this.form.value;

    const success = (value: any) => {
      this.toastr.success(value.message);
      this.form.reset();
      // this.gotoLogin();
    }
    this.auth.copierChangePassword(data, success);
  }
  logout(){
    localStorage.clear();
    this.navCtrl.goTo('/auth/login');
      }


}
