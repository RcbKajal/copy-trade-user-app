import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ChangePasswordPage } from './change-password.page';
import { ChangePasswordPageRoutingModule } from './change-password-routing.module';
import { CommonModule } from '@angular/common';
import { HeaderPageModule } from 'src/app/home/header/header.module';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ChangePasswordPageRoutingModule,
    HeaderPageModule
  ],
  declarations: [ChangePasswordPage]
})
export class ChangePasswordPageModule {}
