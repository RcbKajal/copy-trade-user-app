import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/controller/auth/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.page.html',
  styleUrls: ['./header.page.scss'],
})
export class HeaderPage implements OnInit {

  constructor(private auth: AuthenticationService) { }

  ngOnInit() {
  }


  logout() {
    this.auth.logout();
  }

}
