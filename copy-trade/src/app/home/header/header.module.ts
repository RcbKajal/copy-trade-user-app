import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HeaderPage } from './header.page';
import { HeaderPageRoutingModule } from './header-routing.module';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HeaderPageRoutingModule
  ],
  exports: [
    HeaderPage
  ],
  declarations: [HeaderPage]
})
export class HeaderPageModule {}
