import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewProviderPage } from './new-provider.page';

const routes: Routes = [
  {
    path: '',
    component: NewProviderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewProviderPageRoutingModule {}
