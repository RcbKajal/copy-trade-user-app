import { CommonModule } from '@angular/common';
import { DirectiveModule } from 'src/controller/directive/directive.module';
import { FormsModule } from '@angular/forms';
import { HeaderPageModule } from 'src/app/home/header/header.module';
import { IonicModule } from '@ionic/angular';
import { NewProviderPage } from './new-provider.page';
import { NewProviderPageRoutingModule } from './new-provider-routing.module';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DirectiveModule,
    NewProviderPageRoutingModule,
    HeaderPageModule
  ],
  declarations: [NewProviderPage]
})
export class NewProviderPageModule {}
