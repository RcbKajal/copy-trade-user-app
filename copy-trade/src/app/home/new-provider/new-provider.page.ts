import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from 'src/controller/auth/authentication.service';
import { NavigationRouteService } from 'src/controller/utility/handler/navigation-route.service';
import { StateService } from 'src/controller/app/data/state.service';

@Component({
  selector: 'app-new-provider',
  templateUrl: './new-provider.page.html',
  styleUrls: ['./new-provider.page.scss'],
})
export class NewProviderPage implements OnInit {

  provider: any = []

  constructor(private navCtrl: NavigationRouteService, public auth: AuthenticationService, public stateService: StateService) { }


  ngOnInit() {
    if (this.navCtrl.navigationData !== null) {
      const data = this.navCtrl.navigationData.data;
      this.provider = data;
    }
  }
}
